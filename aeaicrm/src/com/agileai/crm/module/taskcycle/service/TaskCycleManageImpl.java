package com.agileai.crm.module.taskcycle.service;

import com.agileai.crm.cxmodule.TaskCycleManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class TaskCycleManageImpl
        extends StandardServiceImpl
        implements TaskCycleManage {
    public TaskCycleManageImpl() {
        super();
    }

	@Override
	public DataRow getTaskReviewIdRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getTaskReviewIdRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}
}
