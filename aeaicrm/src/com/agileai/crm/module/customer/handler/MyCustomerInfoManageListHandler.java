package com.agileai.crm.module.customer.handler;

import java.util.List;
import java.util.Map;

import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.cxmodule.CustomerGroup8ContentManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.ListUtil;

public class MyCustomerInfoManageListHandler
        extends StandardListHandler {
    public MyCustomerInfoManageListHandler() {
        super();
        this.editHandlerClazz = MyCustomerInfoEditHandler.class;
       
    }
    
	public ViewRenderer prepareDisplay(DataParam param){
		CustomerGroup8ContentManage customerGroup8ContentManage = this.lookupService(CustomerGroup8ContentManage.class);
		User user = (User) getUser();
		PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
		if(!privilegeHelper.isSalesDirector()){
			param.put("salManCode", user.getUserCode());
		}
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = customerGroup8ContentManage.findMyCustomerRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
        setAttribute("custLevel",
                     FormSelectFactory.create("CUST_LEVEL")
                                      .addSelectedValue(param.get("custLevel")));
        setAttribute("custState",
                     FormSelectFactory.create("CUST_STATE")
                                      .addSelectedValue(param.get("custState")));
        setAttribute("visitEffect",
                FormSelectFactory.create("VISIT_EFFECT")
                                 .addSelectedValue(param.get("visitEffect")));
        initMappingItem("CUST_LEVEL",
                FormSelectFactory.create("CUST_LEVEL").getContent());
        initMappingItem("CUST_STATE",
                        FormSelectFactory.create("CUST_STATE").getContent());
        initMappingItem("VISIT_EFFECT",
                        FormSelectFactory.create("VISIT_EFFECT").getContent());
    }
    
    public ViewRenderer doDeleteAction(DataParam param){
    	CustomerGroup8ContentManage customerGroup8ContentManage = this.lookupService(CustomerGroup8ContentManage.class);
		DataParam dataParam = new DataParam("CUST_ID",param.get("CUST_ID"));
		List<DataRow> personRecords = customerGroup8ContentManage.findPersonRecords(dataParam);
		if (!ListUtil.isNullOrEmpty(personRecords)){
			this.setErrorMsg("存在联系人信息,不能删除!");
			return prepareDisplay(param);
		}
		DataParam dataParam1 = new DataParam("custId",param.get("CUST_ID"));
		List<DataRow> salesRecords = customerGroup8ContentManage.findSalesRecords(dataParam1);
		if (!ListUtil.isNullOrEmpty(salesRecords)){
			this.setErrorMsg("存在关联销售信息,不能删除!");
			return prepareDisplay(param);
		}
		DataRow sales = customerGroup8ContentManage.getStatisticsRecords(dataParam1);
		int visitNum = Integer.parseInt(sales.stringValue("STATIS_VISIT"));
		int oppNum = Integer.parseInt(sales.stringValue("STATIS_OPP"));
		int orderNum = Integer.parseInt(sales.stringValue("STATIS_ORDER"));
		
		if (visitNum > 0 || oppNum > 0 || orderNum > 0 ){
			this.setErrorMsg("该客户已被引用,不能删除!");
			return prepareDisplay(param);
		}
		
		TreeAndContentManage service = customerGroup8ContentManage;
		String tabId = "CustomerInfo";
		String columnId = param.get("curColumnId");
		Map<String,String> tabIdAndColFieldMapping = service.getTabIdAndColFieldMapping();
		String colField = tabIdAndColFieldMapping.get(tabId);
		param.put(colField,columnId);
		service.deletContentRecord(tabId,param);	
		return prepareDisplay(param);
	}    

    protected void initParameters(DataParam param) {
        initParamItem(param, "custLevel", "");
        initParamItem(param, "custState", "");
        initParamItem(param, "custSaleMan", "");
        initParamItem(param, "visitEffect", "");
    }
}
