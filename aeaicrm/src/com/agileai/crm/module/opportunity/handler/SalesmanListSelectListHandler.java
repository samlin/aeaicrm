package com.agileai.crm.module.opportunity.handler;

import com.agileai.crm.cxmodule.SalesmanListSelect;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.PickFillModelHandler;

public class SalesmanListSelectListHandler
        extends PickFillModelHandler {
    public SalesmanListSelectListHandler() {
        super();
        this.serviceId = buildServiceId(SalesmanListSelect.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "USER_NAME", "");
    }

    protected SalesmanListSelect getService() {
        return (SalesmanListSelect) this.lookupService(this.getServiceId());
    }
}
