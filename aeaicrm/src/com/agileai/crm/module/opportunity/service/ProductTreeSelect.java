package com.agileai.crm.module.opportunity.service;

import com.agileai.hotweb.bizmoduler.core.TreeSelectService;

public interface ProductTreeSelect
        extends TreeSelectService {
}
