package com.agileai.crm.module.mytasks.exteral;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.cxmodule.ColdCallsManage;
import com.agileai.crm.cxmodule.FollowUpManage;
import com.agileai.crm.cxmodule.MyCustomerSelect;
import com.agileai.crm.cxmodule.ProCustomerSelect;
import com.agileai.crm.cxmodule.TaskCycle8ContentManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.ws.BaseRestService;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;


public class MyTasksImpl extends BaseRestService implements MyTasks {
	
    protected ColdCallsManage coldCallsService() {
        return (ColdCallsManage) this.lookupService(ColdCallsManage.class);
    }
    
    protected TaskCycle8ContentManage taskCycleService() {
        return (TaskCycle8ContentManage) this.lookupService(TaskCycle8ContentManage.class);
    }
    
    protected FollowUpManage followUpService() {
        return (FollowUpManage) this.lookupService(FollowUpManage.class);
    }
    
    protected ProCustomerSelect proCustomerService() {
        return (ProCustomerSelect) this.lookupService(ProCustomerSelect.class);
    }
    
    protected MyCustomerSelect  myCustomerService() {
        return (MyCustomerSelect) this.lookupService(MyCustomerSelect.class);
    }
    
	@Override
	public String findWorkCheckList() {
		String responseText = null;
		try {
			User user = (User) getUser();
			String userId = user.getUserId();
			List<DataRow> rsList = taskCycleService().findTreeRecords(new DataParam("SALE_ID",userId));
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				DataRow dataRow = rsList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("tcPeriod", dataRow.get("TC_PERIOD"));
				jsonObject1.put("taskReviewId", dataRow.get("TASK_REVIEW_ID"));
				jsonObject1.put("taskReviewState", dataRow.get("TASK_REVIEW_STATE"));
				jsonArray.put(jsonObject1);
			}
			
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (JSONException e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String findPotentialCustList(String infoParam) {
		String responseText = null;
		try {
			JSONObject infoParamObject = new JSONObject(infoParam);
			String taskReviewId = infoParamObject.getString("taskReviewId");
			String name = infoParamObject.getString("name");
			User user = (User) getUser();
			DataParam param = new DataParam();
			param.put("TASK_REVIEW_ID", taskReviewId);
			param.put("ORG_SALESMAN", user.getUserId());
			if(!"".equals(name)){
				param.put("orgName", name);
			}
			
			List<DataRow> rsList = proCustomerService().queryPickFillRecords(param);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				DataRow dataRow = rsList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("orgId", dataRow.get("ORG_ID"));
				jsonObject1.put("orgName", dataRow.get("ORG_NAME"));
				jsonArray.put(jsonObject1);
			}
			
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String createStrangeTaskRecord(String taskReviewId,
			String taskReviewState, String orgIds) {
		String responseText = "fail";
    	String[] orgIdsArray = orgIds.split(",");
    	String dateStr = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, new Date());
    	String taskFollowState = "";
    	if("Init".equals(taskReviewState)){
    		taskFollowState = "NoFollowUp";
    	}else{
    		taskFollowState = "Init";
    	}
    	for(int i=0;i < orgIdsArray.length;i++){
    		String orgId = orgIdsArray[i];
    		DataRow dataRow = coldCallsService().getProCustRecord(new DataParam("ORG_ID",orgId));
    		DataParam myTasksParam = new DataParam();
    		myTasksParam.put("TASK_ID", KeyGenerator.instance().genKey());
    		myTasksParam.put("ORG_ID", orgId);
    		myTasksParam.put("CUST_ID", dataRow.get("CUST_ID"));
    		myTasksParam.put("TASK_REVIEW_ID", taskReviewId);
    		myTasksParam.put("SALE_ID", dataRow.get("ORG_SALESMAN"));
    		myTasksParam.put("TASK_FOLLOW_STATE", taskFollowState);
    		myTasksParam.put("TASK_CLASS", "ColdCalls");
    		myTasksParam.put("TASK_CREATE_TIME", dateStr);
    		myTasksParam.put("TASK_FINISH_TIME", "");
    		myTasksParam.put("TASK_CUST_STATE", "");
    		coldCallsService().createMasterRecord(myTasksParam);
    	}
    	
    	responseText = "success";
    	return responseText;
    }
	
	@Override
	public String findStrangeVisitList(String taskReviewId) {
		String responseText = null;
		try {
			User user = (User) getUser();
			List<DataRow> rsList = coldCallsService().findMasterRecords(new DataParam("TASK_REVIEW_ID", taskReviewId,"orgSalesman" ,user.getUserId()));
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				DataRow dataRow = rsList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("taskId", dataRow.get("TASK_ID"));
				jsonObject1.put("orgId", dataRow.get("ORG_ID"));
				jsonObject1.put("name", dataRow.get("ORG_NAME"));
				String classification = FormSelectFactory.create("ORG_CLASSIFICATION").getText(dataRow.getString("ORG_CLASSIFICATION"));
				jsonObject1.put("classification", classification);
				String state = FormSelectFactory.create("PER_STATE").getText(dataRow.getString("ORG_STATE"));
				jsonObject1.put("state", state);
				jsonObject1.put("orgCreaterName", dataRow.get("ORG_CREATER_NAME"));
				jsonObject1.put("updateTime", dataRow.get("ORG_UPDATE_TIME"));
				String labels = FormSelectFactory.create("ORG_LABELS").getText(dataRow.getString("ORG_LABELS"));
				jsonObject1.put("labels", labels);
				String followState = FormSelectFactory.create("TASK_FOLLOW_STATUS").getText(dataRow.getString("TASK_FOLLOW_STATE"));
				jsonObject1.put("followState", followState);
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (JSONException e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String getStrangeVisitInfo(String id) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow dataRow = coldCallsService().getMasterRecord(new DataParam("TASK_ID",id));
			String orgId = dataRow.getString("ORG_ID");
			jsonObject.put("orgId", orgId);
			jsonObject.put("orgName", dataRow.get("ORG_NAME"));
			jsonObject.put("orgClassification", dataRow.get("ORG_CLASSIFICATION"));
			jsonObject.put("orgType", dataRow.get("ORG_TYPE"));
			jsonObject.put("orgSources", dataRow.get("ORG_SOURCES"));
			jsonObject.put("orgLinkmanName", dataRow.get("ORG_LINKMAN_NAME"));
			jsonObject.put("orgEmail", dataRow.get("ORG_EMAIL"));
			jsonObject.put("orgLabels", dataRow.get("ORG_LABELS"));
			jsonObject.put("orgWebsite", dataRow.get("ORG_WEBSITE"));
			jsonObject.put("orgSalesman", dataRow.get("ORG_SALESMAN"));
			jsonObject.put("orgSalesmanName", dataRow.get("ORG_SALESMAN_NAME"));
			jsonObject.put("orgState", dataRow.get("ORG_STATE"));
			jsonObject.put("orgCreater", dataRow.get("ORG_CREATER"));
			jsonObject.put("orgCreaterName", dataRow.get("ORG_CREATER_NAME"));
			jsonObject.put("orgCreateTime", dataRow.get("ORG_CREATE_TIME"));
			jsonObject.put("orgUpdateTime", dataRow.get("ORG_UPDATE_TIME"));
			jsonObject.put("orgContactWay", dataRow.get("ORG_CONTACT_WAY"));
			jsonObject.put("orgAddress", dataRow.get("ORG_ADDRESS"));
			jsonObject.put("orgIntroduction", dataRow.get("ORG_INTRODUCTION"));
			jsonObject.put("orgVisitAgainTime", dataRow.get("ORG_VISIT_AGAIN_TIME"));
			
			List<DataRow> rsList = coldCallsService().findSubRecords("ProCustVisitInfo", new DataParam("ORG_ID",orgId));
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				DataRow dataRow1 = rsList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", dataRow1.get("PROCUST_VISIT_ID"));
				String date = DateUtil.format(DateUtil.YYMMDD_HORIZONTAL, dataRow1.getTimestamp("PROCUST_VISIT_DATE"));
				jsonObject1.put("date", date);
				jsonObject1.put("fillName", dataRow1.get("PROCUST_VISIT_FILL_NAME"));
				String fillTime = DateUtil.format(DateUtil.YYMMDDHHMI_HORIZONTAL, dataRow1.getTimestamp("PROCUST_VISIT_FILL_TIME"));
				jsonObject1.put("fillTime", fillTime);
				String effect = FormSelectFactory.create("VISIT_EFFECT").getText(dataRow1.getString("PROCUST_VISIT_EFFECT"));
				jsonObject1.put("effect", effect);
				String type = FormSelectFactory.create("VISIT_TYPE").getText(dataRow1.getString("PROCUST_VISIT_TYPE"));
				jsonObject1.put("type", type);
				jsonObject1.put("category", dataRow1.get("CUST_VISIT_CATEGORY"));
				
				jsonArray.put(jsonObject1);
			}
			
			jsonObject.put("visitList", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String getStrangeVisitRecordInfo(String id) {
		String responseText = null;
		try {
			DataRow dataRow = coldCallsService().getSubRecord("ProCustVisitInfo", new DataParam("PROCUST_VISIT_ID",id));
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", dataRow.get("PROCUST_VISIT_ID"));
			jsonObject.put("date", dataRow.get("PROCUST_VISIT_DATE"));
			String type = FormSelectFactory.create("VISIT_TYPE").getText(dataRow.getString("PROCUST_VISIT_TYPE"));
			jsonObject.put("type", type);
			String effect = FormSelectFactory.create("VISIT_EFFECT").getText(dataRow.getString("PROCUST_VISIT_EFFECT"));
			jsonObject.put("effect", effect);
			jsonObject.put("fillName", dataRow.get("PROCUST_VISIT_FILL_NAME"));
			String fillTime = DateUtil.format(DateUtil.YYMMDDHHMI_HORIZONTAL, dataRow.getTimestamp("PROCUST_VISIT_FILL_TIME"));
			jsonObject.put("fillTime", fillTime);
			jsonObject.put("remark", dataRow.get("PROCUST_VISIT_REMARK"));
			jsonObject.put("custFocus", dataRow.get("PROCUST_VISIT_CUST_FOCUS"));
			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String findMyCustList(String infoParam) {
		String responseText = null;
		try {
			JSONObject infoParamObject = new JSONObject(infoParam);
			String taskReviewId = infoParamObject.getString("taskReviewId");
			String name = infoParamObject.getString("name");
			User user = (User) getUser();
			DataParam param = new DataParam();
			param.put("TASK_REVIEW_ID", taskReviewId);
			param.put("CUST_SALESMAN", user.getUserId());
			if(!"".equals(name)){
				param.put("custName", name);
			}
			
			List<DataRow> rsList = myCustomerService().queryPickFillRecords(param);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				DataRow dataRow = rsList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("custId", dataRow.get("CUST_ID"));
				jsonObject1.put("custName", dataRow.get("CUST_NAME"));
				jsonArray.put(jsonObject1);
			}
			
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String findIntentionFollowUpList(String taskReviewId) {
		String responseText = null;
		try {
			User user = (User) getUser();
			List<DataRow> reList = followUpService().findMasterRecords(new DataParam("TASK_REVIEW_ID", taskReviewId,"userId", user.getUserId()));
			JSONArray jsonArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			for(int i=0;i<reList.size();i++){
				DataRow dataRow = reList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("taskId", dataRow.get("TASK_ID"));
				jsonObject1.put("custId", dataRow.get("CUST_ID"));
				jsonObject1.put("custName", dataRow.get("CUST_NAME"));
				String custIndustry = FormSelectFactory.create("CUST_INDUSTRY").getText(dataRow.getString("CUST_INDUSTRY"));
				jsonObject1.put("custIndustry", custIndustry);
				String custScale = FormSelectFactory.create("CUST_SCALE").getText(dataRow.getString("CUST_SCALE"));
				jsonObject1.put("custScale", custScale);
				String custNature = FormSelectFactory.create("CUST_NATURE").getText(dataRow.getString("CUST_NATURE"));
				jsonObject1.put("custNature", custNature);
				String custState = FormSelectFactory.create("CUST_STATE").getText(dataRow.getString("CUST_STATE"));
				jsonObject1.put("custState", custState);
				String followState = FormSelectFactory.create("TASK_FOLLOW_STATUS").getText(dataRow.getString("TASK_FOLLOW_STATE"));
				jsonObject1.put("followState", followState);
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (JSONException e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String createIntentionTaskRecord(String taskReviewId, 
			String taskReviewState, String custIds) {
		String responseText = "fail";
    	String[] custIdsArray = custIds.split(",");
    	User user = (User) getUser();
    	String userId = user.getUserId();
    	String dateStr = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, new Date());
    	String taskFollowState = "";
    	if("Init".equals(taskReviewState)){
    		taskFollowState = "NoFollowUp";
    	}else{
    		taskFollowState = "Init";
    	}
    	for(int i=0;i < custIdsArray.length;i++){
    		String custId = custIdsArray[i];
    		DataRow dataRow = followUpService().getMyCustRecord(new DataParam("custId",custId));
    		DataParam myTasksParam = new DataParam();
    		myTasksParam.put("TASK_ID", KeyGenerator.instance().genKey());
    		myTasksParam.put("ORG_ID", dataRow.get("ORG_ID"));
    		myTasksParam.put("CUST_ID", custId);
    		myTasksParam.put("TASK_REVIEW_ID", taskReviewId);
    		myTasksParam.put("SALE_ID", userId);
    		myTasksParam.put("TASK_FOLLOW_STATE", taskFollowState);
    		myTasksParam.put("TASK_CLASS", "FollowUp");
    		myTasksParam.put("TASK_CREATE_TIME", dateStr);
    		myTasksParam.put("TASK_FINISH_TIME", "");
    		myTasksParam.put("TASK_CUST_STATE", "");
    		followUpService().createMasterRecord(myTasksParam);
    	}
    	responseText = "success";
    	return responseText;
    }

	@Override
	public String getIntentionFollowUpInfo(String id) {
		String responseText = null;
		try {
			DataRow dataRow = followUpService().getMasterRecord((new DataParam("TASK_ID",id)));
			JSONObject jsonObject = new JSONObject();
			String custId = dataRow.getString("CUST_ID");
			jsonObject.put("custId", custId);
			jsonObject.put("custName", dataRow.get("CUST_NAME"));
			jsonObject.put("custIndustry", dataRow.get("CUST_INDUSTRY"));
			jsonObject.put("custCompanyWeb", dataRow.get("CUST_COMPANY_WEB"));
			jsonObject.put("custCompanyWeb", dataRow.get("CUST_INDUSTRY"));
			jsonObject.put("custScale", dataRow.get("CUST_SCALE"));
			jsonObject.put("custNature", dataRow.get("CUST_NATURE"));
			jsonObject.put("custLevel", dataRow.get("CUST_LEVEL"));
			jsonObject.put("custProgressState", dataRow.get("CUST_PROGRESS_STATE"));
			jsonObject.put("custAddress", dataRow.get("CUST_ADDRESS"));
			String state = FormSelectFactory.create("CUST_STATE").getText(dataRow.getString("CUST_STATE"));
			jsonObject.put("custState", state);
			jsonObject.put("custIntroduce", dataRow.get("CUST_INTRODUCE"));
			jsonObject.put("custCreateId", dataRow.get("CUST_CREATE_ID"));
			jsonObject.put("custCreateName", dataRow.get("CUST_CREATE_NAME"));
			jsonObject.put("orgId", dataRow.get("ORG_ID"));
			
			List<DataRow> rsList = followUpService().findSubRecords("MyCustVisitInfo", new DataParam("CUST_ID",custId));
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				DataRow dataRow1 = rsList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", dataRow1.get("PROCUST_VISIT_ID"));
				jsonObject1.put("date", dataRow1.get("PROCUST_VISIT_DATE"));
				jsonObject1.put("fillName", dataRow1.get("PROCUST_VISIT_FILL_NAME"));
				String fillTime = DateUtil.format(DateUtil.YYMMDDHHMI_HORIZONTAL, dataRow1.getTimestamp("PROCUST_VISIT_FILL_TIME"));
				jsonObject1.put("fillTime", fillTime);
				String effect = FormSelectFactory.create("VISIT_EFFECT").getText(dataRow1.getString("PROCUST_VISIT_EFFECT"));
				jsonObject1.put("effect", effect);
				String type = FormSelectFactory.create("VISIT_TYPE").getText(dataRow1.getString("PROCUST_VISIT_TYPE"));
				jsonObject1.put("type", type);
				jsonObject1.put("category", dataRow1.get("CUST_VISIT_CATEGORY"));
				
				jsonArray.put(jsonObject1);
			}
			
			jsonObject.put("visitList", jsonArray);
			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String getIntentionVisitRecordInfo(String id) {
		String responseText = null;
		try {
			DataParam param = new DataParam("PROCUST_VISIT_ID", id);
			DataRow dataRow = followUpService().getSubRecord("MyCustVisitInfo", param);
			if(dataRow == null){
				dataRow = followUpService().getCustVisitInfoRecord(param);
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", dataRow.get("PROCUST_VISIT_ID"));
			String date = DateUtil.format(DateUtil.YYMMDD_HORIZONTAL, dataRow.getTimestamp("PROCUST_VISIT_DATE"));
			jsonObject.put("date", date);
			String type = FormSelectFactory.create("VISIT_TYPE").getText(dataRow.getString("PROCUST_VISIT_TYPE"));
			jsonObject.put("type", type);
			String effect = FormSelectFactory.create("VISIT_EFFECT").getText(dataRow.getString("PROCUST_VISIT_EFFECT"));
			jsonObject.put("effect", effect);
			jsonObject.put("fillName", dataRow.get("PROCUST_VISIT_FILL_NAME"));
			String fillTime = DateUtil.format(DateUtil.YYMMDDHHMI_HORIZONTAL, dataRow.getTimestamp("PROCUST_VISIT_FILL_TIME"));
			jsonObject.put("fillTime", fillTime);
			jsonObject.put("remark", dataRow.get("PROCUST_VISIT_REMARK"));
			jsonObject.put("custFocus", dataRow.get("PROCUST_VISIT_CUST_FOCUS"));
			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String suspendInfo(String info) {
		String rspText = "fail";
		try {
			JSONObject jsonObject = new JSONObject(info);
			DataParam param = new DataParam();
			param.put("CUST_ID", jsonObject.getString("custId"));
			param.put("TASK_ID", jsonObject.getString("taskId"));
			param.put("PROCUST_VISIT_REMARK", jsonObject.getString("procustVisitRemark"));
			param.put("PROCUST_VISIT_FILL_TIME", jsonObject.getString("procustVisitFillTime"));
			param.put("PROCUST_VISIT_CUST_FOCUS", jsonObject.getString("procustVisitCustFocus"));
			param.put("PROCUST_VISIT_EFFECT", jsonObject.getString("procustVisitEffect"));
			param.put("PROCUST_VISIT_TYPE", jsonObject.getString("procustVisitType"));
			param.put("PROCUST_VISIT_FILL_ID", jsonObject.getString("procustVisitFillId"));
			
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        	String procustVisitDateUTC = jsonObject.get("procustVisitDate").toString().replace("Z", " UTC");
        	Date pvd = format.parse(procustVisitDateUTC);
        	String procustVisitDate = DateUtil.format(DateUtil.YYMMDD_HORIZONTAL, pvd);
			param.put("PROCUST_VISIT_DATE", procustVisitDate);
			
			param.put("TASK_REVIEW_ID", jsonObject.getString("taskReviewId"));
			param.put("ORG_ID", jsonObject.getString("orgId"));
			
			User user = (User) getUser();
			String custId = "";
			DataRow dataRow = coldCallsService().getCustInfoRecord(param);
			if(dataRow != null && !dataRow.isEmpty()){
				custId = dataRow.getString("CUST_ID");
			}else{
				custId = jsonObject.getString("custId");
			}
			
			if(StringUtil.isNotNullNotEmpty(custId)){
				DataParam custParam = new DataParam();
				custParam.put("CUST_ID", custId);
				custParam.put("CUST_STATE", "Postpone");
				coldCallsService().updateCustStateRecord(custParam);
			}
			
			if(StringUtil.isNotNullNotEmpty(custId)){
				DataRow proCustRow = coldCallsService().getProCustRecord(new DataParam("CUST_ID",custId));
				if(proCustRow != null && !proCustRow.isEmpty()){
					
				}else{
					String orgId = KeyGenerator.instance().genKey();
					param.put("ORG_ID", orgId);
					String dateTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
					DataParam proCustParam = new DataParam();
					proCustParam.put("ORG_ID", orgId);
					proCustParam.put("ORG_NAME", param.get("CUST_NAME"));
					proCustParam.put("ORG_STATE", "0");
					proCustParam.put("ORG_SALESMAN", user.getUserId());
					proCustParam.put("ORG_CREATER", user.getUserId());
					proCustParam.put("ORG_CREATE_TIME", dateTime);
					proCustParam.put("ORG_UPDATE_TIME", dateTime);
					proCustParam.put("CUST_ID", custId);
					
					coldCallsService().createProCustInfoRecord(proCustParam);
					coldCallsService().updateTaskOrgIdRecord(param);
				}
			}
			
			coldCallsService().createProVisitRecord(param);
			
			DataRow taskRow = coldCallsService().getMasterRecord(param);
			String taskFollowState = taskRow.getString("TASK_FOLLOW_STATE");
	    	if("Init".equals(taskFollowState)){
	    		param.put("TASK_FOLLOW_STATE", "HaveFollowUpInit");
	    	}else{
	    		param.put("TASK_FOLLOW_STATE", "HaveFollowUp");
	    	}
	    	coldCallsService().updateTaskStateRecord(param);
	    	
			param.put("TASK_CLASS", "ColdCalls");
			coldCallsService().updateTaskClassRecord(param);
			
			rspText = "success";
		} catch (JSONException e) {
			log.error(e.getLocalizedMessage(),e);
		} catch (ParseException e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return rspText;
	}

	@Override
	public String followUpInfo(String info) {
		String rspText = "fail";
		try {
			JSONObject jsonObject = new JSONObject(info);
			DataParam param = new DataParam();
			param.put("VISIT_FILL_TIME", jsonObject.getString("visitFillTime"));
			param.put("VISIT_CUST_FOCUS", jsonObject.getString("visitCustFocus"));
			param.put("VISIT_RECEPTION_NAME", jsonObject.getString("visitReceptionName"));
			param.put("VISIT_FILL_NAME", jsonObject.getString("visitFillName"));
			param.put("CONT_ID", jsonObject.getString("contId"));
			param.put("TASK_ID", jsonObject.getString("taskId"));
			param.put("VISIT_PEER_NAME", jsonObject.getString("visitPeerName"));
			param.put("ORG_ID", jsonObject.get("orgId"));
			param.put("ORG_NAME", jsonObject.get("orgName"));
			param.put("VISIT_CONTENT", jsonObject.getString("visitContent"));
			param.put("VISIT_DATE", jsonObject.getString("visitDate"));
			param.put("VISIT_IMPROVEMENT", jsonObject.getString("visitImprovement"));
			param.put("VISIT_TYPE", jsonObject.getString("visitType"));
			param.put("VISIT_EFFECT", jsonObject.getString("visitEffect"));
			param.put("VISIT_COST_EXPLAIN", jsonObject.getString("visitCostExplain"));
			param.put("VISIT_USER_ID", jsonObject.getString("visitUserId"));
			param.put("VISIT_COST", jsonObject.getString("visitCost"));
			param.put("TASK_REVIEW_ID", jsonObject.getString("taskReviewId"));
			param.put("VISIT_FILL_ID", jsonObject.getString("visitFillId"));
			param.put("VISIT_STATE", jsonObject.getString("visitState"));
			
			BigDecimal orderMoney = new BigDecimal("0.00");
			if(param.get("VISIT_COST").trim().equals("")){
				param.put("VISIT_COST",orderMoney);
			}
			
			DataRow taskRow = followUpService().getMasterRecord(param);
			String taskFollowState = taskRow.getString("TASK_FOLLOW_STATE");
	    	if("Init".equals(taskFollowState)){
	    		param.put("TASK_FOLLOW_STATE", "HaveFollowUpInit");
	    	}else{
	    		param.put("TASK_FOLLOW_STATE", "HaveFollowUp");
	    	}
			
	    	followUpService().updateTaskStateRecord(param);
			param.put("TASK_CLASS", "FollowUp");
			followUpService().updateTaskClassRecord(param);
			
			String custId = jsonObject.getString("custId");
			param.put("VISIT_CUST_ID",custId);
			
			if(StringUtil.isNotNullNotEmpty(custId)){
				DataParam custParam = new DataParam();
				custParam.put("CUST_ID", custId);
				custParam.put("CUST_STATE", "Confirm");
				followUpService().updateCustStateRecord(custParam);
				followUpService().createCustVisitRecord(param);
			}else{
				String custUuid = KeyGenerator.instance().genKey();
				param.put("custUuid", custUuid);
				followUpService().createCustRecord(param);
				param.put("VISIT_CUST_ID", custUuid);
				followUpService().createCustVisitRecord(param);
			}
			
			rspText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return rspText;
	}

	@Override
	public String getSummaryInfo(String taskReviewId) {
		String rspText = null;
		try {
			DataParam param = new DataParam();
			JSONObject jsonObject = new JSONObject();
			User user = (User) getUser();
			
			param.put("TASK_REVIEW_ID", taskReviewId);
			DataRow record = taskCycleService().getSummaryRecord(param);
			
			String state = FormSelectFactory.create("TASK_REVIEW_STATE").getText(record.getString("TASK_REVIEW_STATE"));
			jsonObject.put("taskReviewState", state);
			jsonObject.put("taskReviewVisitsTotal", record.get("TASK_REVIEW_VISITS_TOTAL"));
			jsonObject.put("taskReviewStrange", record.get("TASK_REVIEW_STRANGE"));
			jsonObject.put("taskReviewDesc", record.get("TASK_REVIEW_DESC"));
			jsonObject.put("taskReviewStrangeTask", record.get("TASK_REVIEW_STRANGE_TASK"));
			jsonObject.put("taskReviewStrangeFollow", record.get("TASK_REVIEW_STRANGE_FOLLOW"));
			jsonObject.put("taskReviewStrangeVisit", record.get("TASK_REVIEW_STRANGE_VISIT"));
			jsonObject.put("taskReviewIntentionTask", record.get("TASK_REVIEW_INTENTION_TASK"));
			jsonObject.put("taskReviewIntentionFollow", record.get("TASK_REVIEW_INTENTION_FOLLOW"));
			jsonObject.put("taskReviewIntentionVisit", record.get("TASK_REVIEW_INTENTION_VISIT"));
			
			param.put("SALE_ID",record.get("SALE_ID"),"TC_ID",record.get("TC_ID"));
			DataRow proNumRow = taskCycleService().getProNumRecord(param);
			long proNum = (Long) proNumRow.get("PRO_NUM");
			jsonObject.put("proNum", proNum);
			
			param.put("SALE_ID",record.get("SALE_ID"),"TC_ID",record.get("TC_ID"));
			DataRow custNumRow = taskCycleService().getCustNumRecord(param);
			long custNum = (Long) custNumRow.get("CUST_NUM");
			jsonObject.put("custNum", custNum);
			
			List<DataRow> oppRecords = taskCycleService().queryOppRecords(new DataParam("TC_ID", record.get("TC_ID"), "SALE_ID", user.getUserId()));
			jsonObject.put("oppNum", oppRecords.size());
			JSONArray oppRecordsArray = new JSONArray();
			for(int i=0;i<oppRecords.size();i++){
				DataRow dataRow = oppRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("oppName", dataRow.getString("OPP_NAME"));
				jsonObject1.put("custIdName", dataRow.getString("CUST_ID_NAME"));
				jsonObject1.put("contIdName", dataRow.getString("CONT_ID_NAME"));
				jsonObject1.put("oppConcernProduct", dataRow.getString("OPP_CONCERN_PRODUCT"));
				jsonObject1.put("clueSalesmanName", dataRow.getString("CLUE_SALESMAN_NAME"));
				jsonObject1.put("oppCreaterName", dataRow.getString("OPP_CREATER_NAME"));
				jsonObject1.put("oppCreateTime", dataRow.getString("OPP_CREATE_TIME"));
				oppRecordsArray.put(jsonObject1);
			}
			jsonObject.put("oppRecords", oppRecordsArray);
			
			List<DataRow> orderRecords = taskCycleService().queryOrderRecords(new DataParam("TC_ID", record.get("TC_ID"), "SALE_ID", user.getUserId()));
			jsonObject.put("orderNum", orderRecords.size());
			JSONArray orderRecordsArray = new JSONArray();
			for(int i=0;i<orderRecords.size();i++){
				DataRow dataRow = orderRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("oppIdName", dataRow.getString("OPP_ID_NAME"));
				jsonObject1.put("orderName", dataRow.getString("ORDER_NAME"));
				jsonObject1.put("orderChief", dataRow.getString("ORDER_CHIEF"));
				jsonObject1.put("orderCost", dataRow.get("ORDER_COST"));
				jsonObject1.put("clueSalesmanName", dataRow.getString("CLUE_SALESMAN_NAME"));
				jsonObject1.put("orderCreaterName", dataRow.getString("ORDER_CREATER_NAME"));
				jsonObject1.put("orderCreateTime", dataRow.getString("ORDER_CREATE_TIME"));
				orderRecordsArray.put(jsonObject1);
			}
			jsonObject.put("orderRecords", orderRecordsArray);
			
			rspText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		
		return rspText;
	}

	@Override
	public String saveSummaryInfo(String taskReviewId,String orderNum,String oppNum) {
		String rspText = "fail";
		DataParam param = new DataParam();
		/*param.put("TASK_REVIEW_DESC",taskReviewDesc);*/
		param.put("TASK_REVIEW_ID",taskReviewId);
		taskCycleService().aggregateyRecord(taskReviewId,orderNum,oppNum);
		rspText = "success";
		return rspText;
	}

	@Override
	public String updateStateInfo(String taskReviewId, String taskReviewState) {
		String rspText = "fail";
		DataParam param = new DataParam();
		param.put("TASK_REVIEW_ID", taskReviewId);
		param.put("TASK_REVIEW_STATE", taskReviewState);
		
		taskCycleService().updatePlanRecord(taskReviewId,taskReviewState);
		rspText = "success";
		return rspText;
	}

	@Override
	public String NoFollowUpInfo(String taskReviewId, String orgId) {
		String rspText = "fail";
		DataParam param = new DataParam();
		param.put("ORG_ID", orgId);
		param.put("TASK_FOLLOW_STATE", "HaveFollowUp");
		coldCallsService().updateTaskStateRecord(param);
		param.put("ORG_STATE", "1");
		coldCallsService().updateTaskOrgStateRecord(param);
		rspText = "success";
		return rspText;
	}

	@Override
	public String findHomeCardRecords() {
		String rspText = null;
		try {
			String nowDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, new Date());
			List<DataRow> rsList = taskCycleService().findHomeCardRecords(new DataParam("nowDate", nowDate));
			JSONArray jsonArray = new JSONArray();
			JSONObject josnObjectHomeCard = new JSONObject();
			String taskReviewId = "";
			String tcPeriod = "";
			for(int i=0;i<rsList.size();i++){
				JSONObject josnObject = new JSONObject();
				DataRow dataRow = rsList.get(i);
				josnObject.put("taskId", dataRow.get("TASK_ID"));
				String taskFollowState = FormSelectFactory.create("TASK_FOLLOW_STATUS").getText(dataRow.getString("TASK_FOLLOW_STATE"));
				josnObject.put("taskFollowState", taskFollowState);
				josnObject.put("custName", dataRow.get("CUST_NAME"));
				josnObject.put("userName", dataRow.get("USER_NAME"));
				jsonArray.put(josnObject);
				taskReviewId =  dataRow.stringValue("TASK_REVIEW_ID");
				tcPeriod =  dataRow.stringValue("TC_PERIOD");
			}
			josnObjectHomeCard.put("taskReviewId",taskReviewId);
			josnObjectHomeCard.put("tcPeriod", tcPeriod);
			josnObjectHomeCard.put("homeCardData", jsonArray);
			rspText = josnObjectHomeCard.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return rspText;
	}
}
