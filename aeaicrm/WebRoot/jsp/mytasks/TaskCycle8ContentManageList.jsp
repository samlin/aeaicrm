<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String currentSubTableId = pageBean.getStringValue("currentSubTableId");
String currentSubTableIndex = pageBean.getStringValue("currentSubTableIndex");
%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var operaRequestBox;
function openContentRequestBox(operaType,title,handlerId,subPKField){
	if ('insert' != operaType && !isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!operaRequestBox){
		operaRequestBox = new PopupBox('operaRequestBox',title,{size:'big',top:'3px'});
	}
	var columnIdValue = $("#columnId").val();
	var url = 'index?'+handlerId+'&TC_ID='+columnIdValue+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	operaRequestBox.sendRequest(url);
}
function showFilterBox(){
	$('#filterBox').show();
	var clientWidth = $(document.body).width();
	var tuneLeft = (clientWidth - $("#filterBox").width())/2-2;	
	$("#filterBox").css('left',tuneLeft);	
}
function doRemoveContent(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要移除该条记录吗？',function(r){
		if(r){
			postRequest('form1',{actionType:'isLastRelation',onComplete:function(responseText){
				if (responseText == 'true'){
					if (confirm('该信息只有一条关联记录，确认要删除吗？')){
						
					}
					jConfirm('该信息只有一条关联记录，确认要删除吗？',function(r){
						if(r){
							doSubmit({actionType:'delete'});
						}
					});
				}else{
					doSubmit({actionType:'removeContent'});
				}
			}});
		}
	});
}

function isSelectedTree(){
	if (isValid($('#columnId').val())){
		return true;
	}else{
		return false;
	}
}
function refreshTree(){
	doQuery();
}
function refreshContent(curNodeId){
	if (curNodeId){
		$('#columnId').val(curNodeId);
	}
	doSubmit({actionType:'query'});
}
var targetTreeBox;
function openTargetTreeBox(curAction){
	var columnIdValue = $("#columnId").val();
	if (!isSelectedTree()){
		writeErrorMsg('请先选中一个树节点!');
		return;
	}
	if (curAction == 'copyContent' || curAction == 'moveContent'){
		if (!isSelectedRow()){
			writeErrorMsg('请先选中一条记录!');
			return;
		}
		columnIdValue = $("#curColumnId").val()
	}	
	if (!targetTreeBox){
		targetTreeBox = new PopupBox('targetTreeBox','请选择目标分组',{size:'normal',width:'300px',top:'3px'});
	}
	var handlerId = "TaskCycle8ContentManageTreePick";
	var url = 'index?'+handlerId+'&TC_ID='+columnIdValue;
	targetTreeBox.sendRequest(url);
	$("#actionType").val(curAction);
}
function doChangeParent(){
	var curAction = $('#actionType').val();
	postRequest('form1',{actionType:curAction,onComplete:function(responseText){
		if (responseText == 'success'){
			if (curAction == 'moveTree'){
				refreshTree();			
			}else{
				refreshContent($("#targetParentId").val());		
			}
		}else {
			writeErrorMsg('迁移父节点出错啦！');
		}
	}});
}
function clearFilter(){
	$("#filterBox input[type!='button'],select").val('');
}
function changeSubTable(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'changeSubTable'});
}
var orgIdBox;
function openProIdBox(){
	var handlerId = "TaskCycleSelect"; 
	if (!orgIdBox){
		orgIdBox = new PopupBox('orgIdBox','请选择周期',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=TC_ID';
	orgIdBox.sendRequest(url);
}
function generatPlan(){
	var confirmMsg = '您确认要选择当前周期吗？';
	jConfirm(confirmMsg,function(r){
		if(r){
			doSubmit({actionType:'generatPlan'});
		}else{
			doSubmit({actionType:'prepareDisplay'});
		}
	});
}
function delPlan(){
	var confirmMsg = '您确认要删除当前计划吗？';
	jConfirm(confirmMsg,function(r){
		if(r){
			$('#columnId').val();
			doSubmit({actionType:'delPlan'});
		}
	});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;">
<tr>
	<td valign="top">
    <div id="leftTree" class="sharp color2" style="margin-top:0px;width:200px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3 class="portletTitle">&nbsp;&nbsp;周期列表 </h3>        
	<table id="_TreeToolBar_" border="0" cellpadding="0" cellspacing="0" >
    <tr>
    	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openProIdBox()"><input value="&nbsp;" title="创建计划" type="button" class="newImgBtn" style="margin-right:0px;" />创建计划</td>
    	<%if(pageBean.getBoolValue("doDel")){%>
    	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="delPlan()"><input value="&nbsp;" title="删除计划" type="button" class="delImgBtn" style="margin-right:0px;" />删除计划</td>
    	<%} %>
    </tr>
    </table>	
    <div id="treeArea" style="overflow:auto; height:500px;width:230px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
    <%=pageBean.getStringValue("menuTreeSyntax")%></div>
    </div>
    <b class="b9"></b>
    </div>
    <input type="hidden" id="columnId" name="columnId" value="<%=pageBean.inputValue("columnId")%>" />
    <input type="hidden" id="targetParentId" name="targetParentId" value="" />
    </td>
	<td width="85%" valign="top">
<div style="">
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div class="photobg1" id="tabHeader">
	<div class="newarticle1" onclick="changeSubTable('ColdCalls')">陌生拜访</div>
	<div class="newarticle1" onclick="changeSubTable('FollowUp')">意向跟进</div>
	<div class="newarticle1" onclick="changeSubTable('WorkSummary')"><%=pageBean.inputValue("thirdTabName")%></div>
</div>

<div class="photobox newarticlebox" id="Layer0" style="height:auto;display:none;overflow:hidden;">
<%
if("ColdCalls".equals(pageBean.inputValue("currentSubTableId"))){
%>
<iframe id="HandlerFrame" src="index?ColdCallsManageList&TASK_REVIEW_ID=<%=pageBean.inputValue("columnId")%>" width="100%" height="550" frameborder="0" scrolling="no"></iframe>
<%} %>
</div>
<div class="photobox newarticlebox" id="Layer1" style="height:auto;display:none;overflow:hidden;">
<%
if("FollowUp".equals(pageBean.inputValue("currentSubTableId"))){
%>
<iframe id="HandlerFrame" src="index?FollowUpManageList&TASK_REVIEW_ID=<%=pageBean.inputValue("columnId")%>" width="100%" height="550" frameborder="0" scrolling="no"></iframe>
<%} %>
</div>
<div class="photobox newarticlebox" id="Layer2" style="height:auto;display:none;overflow:hidden;">
<%
if("WorkSummary".equals(pageBean.inputValue("currentSubTableId"))){
%>
<iframe id="HandlerFrame" src="index?WorkSummary&TASK_REVIEW_ID=<%=pageBean.inputValue("columnId")%>" width="100%" height="550" frameborder="0" scrolling="no"></iframe>
<%} %>
</div>
</div>
</td>
</tr>
</table>
<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus({"id":"<%=pageBean.inputValue("currentLayerleId")%>"});
$(function(){
	resetTreeHeight(78);	
	resetRightAreaHeight(87);
});
</script>
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" id="currentSubTableId" name="currentSubTableId" value="<%=pageBean.inputValue("currentSubTableId")%>" />
<input type="hidden" name="TC_ID" id="TC_ID" value="<%=pageBean.inputValue("TC_ID")%>"/>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
